﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.18">
  <POU Name="MotorSequencing" Id="{df7bf1ff-6f56-4176-b078-8a81bce8070b}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK MotorSequencing
VAR_INPUT
	RunMotors: BOOL;
	StopMotors: BOOL;
	ManualMode: BOOL;
	Reset: BOOL;
END_VAR
VAR_OUTPUT
	SequenceUpComplete : BOOL;
	SequenceDownComplete: BOOL;
END_VAR
VAR
	SirenTimer : TON;
	SirenTime : TIME := T#2S;
	
	CurrentSequenceStepOn :INT := 10;
	CurrentSequenceStepOff:INT := 100;
	DelayStartTimer 	:TON;
	StartDelayPerMotor: TIME	:= T#2S;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// A start sequence and a seperate stop sequence.
//Start sequence CurrentSequenceStepOn , stop sequence CurrentSequenceStepOff

//Control to determine which case statement runs (wheter starting motors or stopping)
IF StopMotors THEN
		CurrentSequenceStepOn := 10;
		SequenceUpComplete := FALSE;
END_IF

IF RunMotors THEN
	CurrentSequenceStepOff := 100;
	SequenceDownComplete := FALSE;
END_IF
	

//Timers
//
// Delay for each step between motors starting and the next step, Currently same for starting and stopping
DelayStartTimer(
	PT := StartDelayPerMotor,
	);
	
SirenTimer(IN:= PhysicalIO.bSiren,
	PT := SirenTime,
	);

//Startup sequence
//
//
CASE CurrentSequenceStepOn OF 
10: //Waiting for start signal
	//
	SequenceUpComplete := FALSE;
	IF RunMotors THEN
		CurrentSequenceStepOn := 11;
	END_IF
	
11: //Blast the siren
	PhysicalIO.bSiren := TRUE;
	IF SirenTimer.Q THEN
		PhysicalIO.bSiren := FALSE;
		CurrentSequenceStepOn := 20;
	END_IF

20: //Startup 1
	//Return Conveyor and Hopper feed conveyor
	// DOL 1 and 6
	IF NOT( PhysicalIO.bDOLStart1 AND PhysicalIO.bDOLForward6) THEN
	
	PhysicalIO.bDOLStart1 := TRUE;
	PhysicalIO.bDOLForward6 := TRUE;
	PhysicalIO.bDOLReverse6 := FALSE;
	DelayStartTimer(IN :=TRUE,);
	CurrentSequenceStepOn := 21;
	ELSE  
		CurrentSequenceStepOn := 30;
	END_IF
	
	
21: IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 30;
	END_IF
	
30: //Startup 2
	//SortBelts
	//VSD 1 and 2
	IF NOT(PhysicalIO.VSDStartArray[0] AND PhysicalIO.VSDStartArray[1]) THEN
	PhysicalIO.VSDStartArray[0] := TRUE;
	PhysicalIO.VSDStartArray[1] := TRUE;
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOn := 31;
	ELSE
		CurrentSequenceStepOn := 40;
	END_IF		
	

31:
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 40;
	END_IF

40: //Startup 3
	//Wash Brushs
	//DOL 3 and 4
	IF NOT (PhysicalIO.bDOLStart3 AND PhysicalIO.bDOLStart4) THEN

	PhysicalIO.bDOLStart3 := TRUE;
	PhysicalIO.bDOLStart4 := TRUE;
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOn := 41;
	ELSE
		CurrentSequenceStepOn := 50;
	END_IF
		
41:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 50;
	END_IF

50: //Startup 4
	//Singulator B and C
	//VSDs 3-10
	IF NOT(PhysicalIO.VSDStartArray[2] AND
	PhysicalIO.VSDStartArray[3] AND
	PhysicalIO.VSDStartArray[4] AND
	PhysicalIO.VSDStartArray[5] AND
	PhysicalIO.VSDStartArray[6] AND
	PhysicalIO.VSDStartArray[7] AND
	PhysicalIO.VSDStartArray[8] AND
	PhysicalIO.VSDStartArray[9]) THEN
		PhysicalIO.VSDStartArray[2] := TRUE;
		PhysicalIO.VSDStartArray[3] := TRUE;
		PhysicalIO.VSDStartArray[4] := TRUE;
		PhysicalIO.VSDStartArray[5] := TRUE;
		PhysicalIO.VSDStartArray[6] := TRUE;
		PhysicalIO.VSDStartArray[7] := TRUE;
		PhysicalIO.VSDStartArray[8] := TRUE;
		PhysicalIO.VSDStartArray[9] := TRUE;
		
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOn := 51;
	ELSE
		 CurrentSequenceStepOn := 60;
	END_IF
 

51:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 60;
	END_IF
	
60: //Startup 5
	//Singulator A belts
	//VSDs 11-14
	IF NOT (PhysicalIO.VSDStartArray[10] AND
		PhysicalIO.VSDStartArray[11] AND
		PhysicalIO.VSDStartArray[12] AND
		PhysicalIO.VSDStartArray[13]) THEN
	PhysicalIO.VSDStartArray[10] := TRUE;
	PhysicalIO.VSDStartArray[11] := TRUE;
	PhysicalIO.VSDStartArray[12] := TRUE;
	PhysicalIO.VSDStartArray[13] := TRUE;
		DelayStartTimer(IN :=TRUE,);
	CurrentSequenceStepOn := 61;
	ELSE
		CurrentSequenceStepOn := 70;
	END_IF

61:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 70;
	END_IF

70: //Startup 6
	//Auger takeoff
	//DOL 5
	IF NOT (PhysicalIO.bDOLStart5)  THEN
	PhysicalIO.bDOLStart5 := TRUE;
	
		DelayStartTimer(IN :=TRUE,);
		 CurrentSequenceStepOn := 71;
		 ELSE
		CurrentSequenceStepOn := 80;
	END_IF

71:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 80;
	END_IF

80: //Startup 7
	//Auger Vartical
	//VSD 15
	IF NOT(PhysicalIO.VSDStartArray[14]) THEN
	PhysicalIO.VSDStartArray[14]:= TRUE;
	
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOn := 81;
ELSE
		CurrentSequenceStepOn := 90;
	END_IF
81:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOn := 90;
	END_IF

90: //Startup 8
	//Auger Horizontal
	//VSD 16 
	IF NOT (PhysicalIO.VSDStartArray[15]) THEN
	PhysicalIO.VSDStartArray[15] := TRUE;
	END_IF
	CurrentSequenceStepOn := 100;

	
100: //Running All
	SequenceUpComplete := TRUE; 
	IF NOT RunMotors THEN
		CurrentSequenceStepOn := 10;
	END_IF
	
END_CASE



//Stop sequence
//
//

CASE CurrentSequenceStepOff OF 
10: //Waiting for start signal
	//
	SequenceDownComplete := TRUE;
	IF NOT StopMotors THEN
		 CurrentSequenceStepOff := 100;
	END_IF

20: //Startup 1
	//Return Conveyor and Hopper feed conveyor
	// DOL 1 and 6
	IF ( PhysicalIO.bDOLStart1 OR PhysicalIO.bDOLForward6) THEN
	
	PhysicalIO.bDOLStart1 := FALSE;
	PhysicalIO.bDOLForward6 := FALSE;
	END_IF
	CurrentSequenceStepOff := 10;
	
	
30: //Startup 2
	//SortBelts
	//VSD 1 and 2
	IF (PhysicalIO.VSDStartArray[0] OR PhysicalIO.VSDStartArray[1]) THEN
	PhysicalIO.VSDStartArray[0] := FALSE;
	PhysicalIO.VSDStartArray[1] := FALSE;
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOff := 31;
	ELSE
		CurrentSequenceStepOff := 20;
	END_IF		
	

31:
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 20;
	END_IF

40: //Startup 3
	//Wash Brushs
	//DOL 3 and 4
	IF (PhysicalIO.bDOLStart3 OR PhysicalIO.bDOLStart4) THEN

	PhysicalIO.bDOLStart3 := FALSE;
	PhysicalIO.bDOLStart4 := FALSE;
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOff := 41;
	ELSE
		CurrentSequenceStepOff := 30;
	END_IF
		
41:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 30;
	END_IF

50: //Startup 4
	//Singulator B and C
	//VSDs 3-10
	IF (PhysicalIO.VSDStartArray[2] OR
	PhysicalIO.VSDStartArray[3] OR
	PhysicalIO.VSDStartArray[4] OR
	PhysicalIO.VSDStartArray[5] OR
	PhysicalIO.VSDStartArray[6] OR
	PhysicalIO.VSDStartArray[7] OR
	PhysicalIO.VSDStartArray[8] OR
	PhysicalIO.VSDStartArray[9]) THEN
		PhysicalIO.VSDStartArray[2] := FALSE;
		PhysicalIO.VSDStartArray[3] := FALSE;
		PhysicalIO.VSDStartArray[4] := FALSE;
		PhysicalIO.VSDStartArray[5] := FALSE;
		PhysicalIO.VSDStartArray[6] := FALSE;
		PhysicalIO.VSDStartArray[7] := FALSE;
		PhysicalIO.VSDStartArray[8] := FALSE;
		PhysicalIO.VSDStartArray[9] := FALSE;
		
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOff := 51;
	ELSE
		 CurrentSequenceStepOff := 40;
	END_IF
 

51:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 40;
	END_IF
	
60: //Startup 5
	//Singulator A belts
	//VSDs 11-14
	IF (PhysicalIO.VSDStartArray[10] OR
		PhysicalIO.VSDStartArray[11] OR
		PhysicalIO.VSDStartArray[12] OR
		PhysicalIO.VSDStartArray[13]) THEN
	PhysicalIO.VSDStartArray[10] := FALSE;
	PhysicalIO.VSDStartArray[11] := FALSE;
	PhysicalIO.VSDStartArray[12] := FALSE;
	PhysicalIO.VSDStartArray[13] := FALSE;
		DelayStartTimer(IN :=TRUE,);
	CurrentSequenceStepOff := 61;
	ELSE
		CurrentSequenceStepOff := 50;
	END_IF

61:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 50;
	END_IF

70: //Startup 6
	//Auger takeoff
	//DOL 5
	IF (PhysicalIO.bDOLStart5)  THEN
	PhysicalIO.bDOLStart5 := FALSE;
	
		DelayStartTimer(IN :=TRUE,);
		 CurrentSequenceStepOff := 71;
		 ELSE
		CurrentSequenceStepOff := 60;
	END_IF

71:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 60;
	END_IF

80: //Startup 7
	//Auger Vartical
	//VSD 15
	IF(PhysicalIO.VSDStartArray[14]) THEN
	PhysicalIO.VSDStartArray[14] := FALSE;
	
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOff := 81;
ELSE
		CurrentSequenceStepOff := 70;
	END_IF
81:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 70;
	END_IF

90: //Startup 8
	//Auger Horizontal
	//VSD 16 
	IF (PhysicalIO.VSDStartArray[15]) THEN
	PhysicalIO.VSDStartArray[15] := FALSE ;
	
		DelayStartTimer(IN :=TRUE,);
		CurrentSequenceStepOff := 91;
		ELSE
		CurrentSequenceStepOff := 80;
	END_IF

91:	
	IF DelayStartTimer.Q THEN
		DelayStartTimer(IN :=FALSE,);
		CurrentSequenceStepOff := 80;
	END_IF
	
100: //Running All
	SequenceDownComplete := FALSE; 
	IF StopMotors THEN
		CurrentSequenceStepOff := 90;
	END_IF
	
END_CASE]]></ST>
    </Implementation>
    <LineIds Name="MotorSequencing">
      <LineId Id="556" Count="0" />
      <LineId Id="559" Count="0" />
      <LineId Id="557" Count="1" />
      <LineId Id="308" Count="1" />
      <LineId Id="311" Count="0" />
      <LineId Id="310" Count="0" />
      <LineId Id="317" Count="3" />
      <LineId Id="316" Count="0" />
      <LineId Id="306" Count="0" />
      <LineId Id="560" Count="0" />
      <LineId Id="148" Count="0" />
      <LineId Id="561" Count="1" />
      <LineId Id="147" Count="0" />
      <LineId Id="149" Count="1" />
      <LineId Id="528" Count="1" />
      <LineId Id="535" Count="0" />
      <LineId Id="531" Count="0" />
      <LineId Id="563" Count="0" />
      <LineId Id="34" Count="0" />
      <LineId Id="564" Count="0" />
      <LineId Id="567" Count="0" />
      <LineId Id="9" Count="0" />
      <LineId Id="23" Count="0" />
      <LineId Id="88" Count="0" />
      <LineId Id="314" Count="0" />
      <LineId Id="25" Count="0" />
      <LineId Id="28" Count="0" />
      <LineId Id="303" Count="0" />
      <LineId Id="522" Count="1" />
      <LineId Id="532" Count="0" />
      <LineId Id="525" Count="0" />
      <LineId Id="533" Count="0" />
      <LineId Id="526" Count="0" />
      <LineId Id="524" Count="0" />
      <LineId Id="300" Count="0" />
      <LineId Id="24" Count="0" />
      <LineId Id="27" Count="0" />
      <LineId Id="102" Count="0" />
      <LineId Id="204" Count="0" />
      <LineId Id="103" Count="2" />
      <LineId Id="521" Count="0" />
      <LineId Id="152" Count="0" />
      <LineId Id="213" Count="0" />
      <LineId Id="205" Count="2" />
      <LineId Id="153" Count="0" />
      <LineId Id="156" Count="0" />
      <LineId Id="209" Count="2" />
      <LineId Id="208" Count="0" />
      <LineId Id="212" Count="0" />
      <LineId Id="26" Count="0" />
      <LineId Id="107" Count="3" />
      <LineId Id="112" Count="0" />
      <LineId Id="159" Count="0" />
      <LineId Id="219" Count="1" />
      <LineId Id="225" Count="0" />
      <LineId Id="221" Count="0" />
      <LineId Id="160" Count="0" />
      <LineId Id="158" Count="0" />
      <LineId Id="46" Count="0" />
      <LineId Id="216" Count="2" />
      <LineId Id="215" Count="0" />
      <LineId Id="214" Count="0" />
      <LineId Id="45" Count="0" />
      <LineId Id="113" Count="0" />
      <LineId Id="237" Count="1" />
      <LineId Id="115" Count="2" />
      <LineId Id="165" Count="0" />
      <LineId Id="240" Count="0" />
      <LineId Id="226" Count="0" />
      <LineId Id="241" Count="1" />
      <LineId Id="239" Count="0" />
      <LineId Id="166" Count="3" />
      <LineId Id="164" Count="0" />
      <LineId Id="48" Count="0" />
      <LineId Id="47" Count="0" />
      <LineId Id="120" Count="0" />
      <LineId Id="245" Count="1" />
      <LineId Id="248" Count="6" />
      <LineId Id="122" Count="0" />
      <LineId Id="50" Count="0" />
      <LineId Id="123" Count="5" />
      <LineId Id="176" Count="0" />
      <LineId Id="171" Count="0" />
      <LineId Id="255" Count="3" />
      <LineId Id="227" Count="1" />
      <LineId Id="172" Count="3" />
      <LineId Id="170" Count="0" />
      <LineId Id="129" Count="0" />
      <LineId Id="49" Count="0" />
      <LineId Id="131" Count="0" />
      <LineId Id="137" Count="0" />
      <LineId Id="132" Count="0" />
      <LineId Id="262" Count="2" />
      <LineId Id="133" Count="3" />
      <LineId Id="178" Count="0" />
      <LineId Id="229" Count="0" />
      <LineId Id="266" Count="2" />
      <LineId Id="230" Count="0" />
      <LineId Id="179" Count="3" />
      <LineId Id="177" Count="0" />
      <LineId Id="52" Count="1" />
      <LineId Id="138" Count="1" />
      <LineId Id="119" Count="0" />
      <LineId Id="118" Count="0" />
      <LineId Id="183" Count="0" />
      <LineId Id="185" Count="0" />
      <LineId Id="259" Count="0" />
      <LineId Id="270" Count="1" />
      <LineId Id="269" Count="0" />
      <LineId Id="231" Count="0" />
      <LineId Id="186" Count="3" />
      <LineId Id="184" Count="0" />
      <LineId Id="55" Count="0" />
      <LineId Id="54" Count="0" />
      <LineId Id="142" Count="0" />
      <LineId Id="140" Count="0" />
      <LineId Id="277" Count="0" />
      <LineId Id="144" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="192" Count="0" />
      <LineId Id="260" Count="0" />
      <LineId Id="272" Count="1" />
      <LineId Id="232" Count="0" />
      <LineId Id="193" Count="3" />
      <LineId Id="191" Count="0" />
      <LineId Id="56" Count="0" />
      <LineId Id="51" Count="0" />
      <LineId Id="143" Count="0" />
      <LineId Id="141" Count="0" />
      <LineId Id="278" Count="0" />
      <LineId Id="58" Count="0" />
      <LineId Id="274" Count="0" />
      <LineId Id="503" Count="0" />
      <LineId Id="198" Count="0" />
      <LineId Id="145" Count="0" />
      <LineId Id="57" Count="0" />
      <LineId Id="312" Count="0" />
      <LineId Id="72" Count="1" />
      <LineId Id="71" Count="0" />
      <LineId Id="74" Count="0" />
      <LineId Id="22" Count="0" />
      <LineId Id="569" Count="2" />
      <LineId Id="568" Count="0" />
      <LineId Id="572" Count="1" />
      <LineId Id="322" Count="15" />
      <LineId Id="342" Count="2" />
      <LineId Id="349" Count="153" />
      <LineId Id="321" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>